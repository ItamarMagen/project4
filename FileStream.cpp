#include "FileStream.h"
#define _CRT_SECURE_NO_WARINGS
FileStream::FileStream()
{
	char* path;
	cout << "Enter your file path:" << endl;
	cin >> path;
	this->f = fopen(path, "w+");
}

FileStream::~FileStream()
{
}

FileStream& FileStream::operator<<(const char *str)
{
	fprintf(this->f, str, "%s");
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	fprintf(this->f ,"%d", num);
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}


void endline(FileStream stream)
{
	fprintf(stream.getFileName() ,"\n");
}

FILE * FileStream::getFileName()
{
	return this->f;
}

