#pragma once

#include "OutStream.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class FileStream
{
protected:
	FILE * f;

public:

	FileStream();
	~FileStream();
	
	FILE * getFileName();

	FileStream& operator<<(const char *str);
	FileStream& operator<<(int num);
	FileStream& operator<<(void(*pf)());
};

void endline(FileStream stream);